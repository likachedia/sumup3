package com.example.sumup3

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User1(val firstName:String, val lastName:String, val email:String): Parcelable {

}