package com.example.sumup3

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
class ItemAdapter(private val context: Context,  val dataset: MutableList<Datas>, val itemActions: ItemActions
): RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val firstName: TextView = view.findViewById(R.id.firstName)
        val lastName: TextView = view.findViewById(R.id.lastName)
        val email: TextView = view.findViewById(R.id.email)
        val imageButton: ImageButton = view.findViewById(R.id.imgButton)
        val imgBtn2:ImageButton = view.findViewById(R.id.imgButton2)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.users_activity, parent, false)

        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.firstName.text = item.firstName
        holder.lastName.text = item.lastName
        holder.email.text = item.email
        holder.imageButton.setImageResource(item.imgButton)
        holder.imgBtn2.setImageResource(item.imgButton2)
        holder.imgBtn2.setOnClickListener {
            dataset.removeAt(position)
            notifyItemRemoved(position)
        }
        holder.imageButton.setOnClickListener {
            itemActions.editItem(position)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = dataset.size
}