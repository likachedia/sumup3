package com.example.sumup3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.text.Html
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView

class UserChangeActivity2 : AppCompatActivity() {
    private lateinit var myDataset: MutableList<Datas>
    private lateinit var firstName:EditText
    private lateinit var lastName:EditText
    private lateinit var eEmail:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_change2)
        val button:ImageButton = findViewById(R.id.imgButton)

        firstName = findViewById(R.id.firstName)
        lastName = findViewById(R.id.lastName)
        eEmail = findViewById(R.id.email)

        this.setDefaultValues()

        myDataset = DataSource().loadDatas()
        button.setOnClickListener {
           save()
        }
    }

   private fun save() {
       val user: Parcelable? = User1(firstName.text.toString(), lastName.text.toString(), eEmail.text.toString())
       intent.putExtra("result", user)

       setResult(RESULT_OK, intent)
       finish()
    }

    private fun setDefaultValues() {
        val user = intent.getParcelableExtra<User1>("user")
        firstName.setText(user?.firstName)
        lastName.setText(user?.lastName)
        eEmail.setText(user?.email)
    }
}