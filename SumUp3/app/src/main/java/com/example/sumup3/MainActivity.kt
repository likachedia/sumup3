package com.example.sumup3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), ItemActions {
    private lateinit var binding: ActivityMainBinding
    private lateinit var myDataset: MutableList<Datas>
    private lateinit var recyclerView: RecyclerView
    private var editItemIndex: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.addUser.setOnClickListener {
            createItem()
        }
        myDataset = DataSource().loadDatas()
        recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerView.adapter = ItemAdapter(this, myDataset, this)
        recyclerView.setHasFixedSize(true)
     //   btn = findViewById(R.id.imgButton)

    /**    btn.setOnClickListener {
            change()
        }
**/

    }

    private fun createItem() {
        val intent = Intent(this, UserChangeActivity2::class.java)
        createItemActivity.launch(intent)
    }

    private val createItemActivity = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == RESULT_OK) {
            val user = it.data?.getParcelableExtra<User1>("result")
            val newItem = Datas(user?.firstName!!, user?.lastName!!, user?.email!!,
                R.drawable.ic_baseline_add_box_24, R.drawable.ic_baseline_remove_circle_24)
            myDataset.add(newItem)

            recyclerView.adapter?.notifyDataSetChanged()
        }
    }

    override fun editItem(index: Int) {
        editItemIndex = index;
        val item = myDataset[editItemIndex]
        val intent = Intent(this, UserChangeActivity2::class.java)
        val user = User1(item.firstName, item.lastName, item.email)
        intent.putExtra("user", user)
        editItemActivity.launch(intent)
    }

    private val editItemActivity = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == RESULT_OK) {
            val user = it.data?.getParcelableExtra<User1>("result")
            myDataset[editItemIndex].firstName = user?.firstName!!
            myDataset[editItemIndex].lastName = user?.lastName!!
            myDataset[editItemIndex].email = user?.email!!

            recyclerView.adapter?.notifyDataSetChanged()
        }
    }
}