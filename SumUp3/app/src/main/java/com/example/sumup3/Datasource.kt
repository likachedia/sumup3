package com.example.sumup3

import android.provider.ContactsContract
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class Datas(
        var firstName:String,
        var lastName:String,
        var email:String,
        var imgButton: Int,
        var imgButton2: Int,
)


class DataSource {
    fun loadDatas():MutableList<Datas> {
        return mutableListOf<Datas>(
           Datas("lika", "chedia", "likachedia@gmail.com", R.drawable.ic_baseline_add_box_24, R.drawable.ic_baseline_remove_circle_24),
            Datas("lika1", "chedia1", "likachedia@gmail.com",  R.drawable.ic_baseline_add_box_24, R.drawable.ic_baseline_remove_circle_24),
            Datas("lika2", "chedia2", "likachedia@gmail.com",  R.drawable.ic_baseline_add_box_24, R.drawable.ic_baseline_remove_circle_24),
            Datas("lika3", "chedia3", "likachedia@gmail.com",  R.drawable.ic_baseline_add_box_24, R.drawable.ic_baseline_remove_circle_24),
            Datas("lika4", "chedia4", "likachedia@gmail.com",  R.drawable.ic_baseline_add_box_24, R.drawable.ic_baseline_remove_circle_24),
        )
    }
}